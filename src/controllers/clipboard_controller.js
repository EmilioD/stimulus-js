import {
    Controller
} from "@hotwired/stimulus"

export default class extends Controller {
    static targets =  ['source']    

    copy() {
        console.log(this.source);
        navigator.clipboard.writeText(this.source).then(() => {
            console.log("Copied to clipboard:", this.source);
        });
    }

    get source() {
        return this.sourceTarget.value
    }
}