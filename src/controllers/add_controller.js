import {
    Controller
} from '@hotwired/stimulus'

export default class extends Controller {
    static targets = ["todo"];

    tarea() {
        console.log(`Task: ${this.todo}`);

        if (this.todo.length == 0) {
            alert("Please enter a task")
        } else {
            document.querySelector('#tasks').innerHTML += `
            <div class="task" data-controller="clipboard">
                <input id="taskname" data-clipboard-target="source" value="${this.todo}" readonly></input>
                <button data-action="clipboard#copy">Copy</button>
                <button class="delete">X
                    <i class="far fa-trash-alt"></i>
                </button>
            </div>
        `;
            var current_tasks = document.querySelectorAll(".delete");
            for (var i = 0; i < current_tasks.length; i++) {
                current_tasks[i].onclick = function () {
                    this.parentNode.remove();
                    console.log("Tarea eliminada");
                }
            }
        }
    }

    get todo() {
        return this.todoTarget.value
    }
}