import { Application } from '@hotwired/stimulus'
import ExampleController from './controllers/example_controller'
import AddController from './controllers/add_controller'
import HelloController from './controllers/hello_controller'
import ClipboardController from './controllers/clipboard_controller'

application = Application.start()
application.register('example', ExampleController)
application.register('add', AddController)
application.register('hello', HelloController)
application.register('clipboard', ClipboardController)
